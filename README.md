# doom-theme-generator

doom-theme-generator is a Python program that "compiles" [Pywal](https://github.com/dylanaraps/pywal) colorschemes into valid themes for hlissner's [emacs-doom-themes](https://github.com/hlissner/emacs-doom-themes).

On an unrelated note, this is a fork of [the original Doom Emacs theme generator](https://gitlab.com/valtrok/doom-theme-generator) that uses [`terminal.sexy`](http://terminal.sexy/) color schemes instead.
Big credits to [valtrok @gitlab](https://gitlab.com/valtrok) for attempting this before me. :)

This script was only created to generate Doom Emacs into static files and nothing else.
If you don't want to put the effort of setting this up, you could always use [ewal](https://gitlab.com/jjzmajic/ewal) (but I didn't use it because IDK how to customize it further).




## Prerequisites

* Python 3 (for future references, I'm using `Python 3.8.2`)
* https://github.com/dylanaraps/pywal[Pywal]




## Setup

The following commands assumes you're using a Linux-based system.

```shell
# Cloning this repo somewhere.
git clone https://gitlab.com/foo-dogsquared/doom-theme-generator.git && cd doom-theme-generator

# Make the script executable just in case.
chmod +x doom-theme-generator.py
```




## Usage

Here are some examples of how to use the Doom Emacs theme generator.

```shell
# Generate a Doom Emacs theme from the default JSON file from the Pywal cache.
# Make sure the JSON files are created using the [default template](https://github.com/dylanaraps/pywal/blob/master/pywal/templates/colors.json).
# The generated Doom Emacs theme will be at `doom-$THEME-theme.el` where `$THEME` is the basename of the 'wallpaper' key from the Pywal JSON file.
./doom-theme-generator.py

# Practically the same command.
./doom-theme-generator.py --input ${PYWAL_CACHE_DIR:-"$HOME/.cache/wal/colors.json"}

# Generate a Doom Emacs theme from `mountain-galaxy.json` with the name `mountain-galaxy` and create it in the `$DOOM_DIR` themes directory directly.
./doom-theme-generator.py --input mountain-galaxy.json --name $(basename mountain-galaxy.json) --output ~/.config/doom/doom-mountain-theme.el
```

After the theme is generated, it is up to you what to do with it.
Usually, you would move the generated theme to your Doom Emacs themes installation directory (`$DOOM_DIR/themes`) like `mv doom-<name_of_the_theme>-theme.el path/to/your/doom-themes`.
Then, add the following line to your Doom Emacs config file (`$DOOM_DIR/config.el`): `(load-theme 'doom-<name_of_the_theme> t)`.

