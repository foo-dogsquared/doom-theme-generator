#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import colorsys
import json
import logging
import sys
from pathlib import Path

import pywal
from pywal.util import Color

COLORS_256_LIST = []


def lighter(color, amt):
    """
    Make the specified Pywal Color object %amt lighter.

    :param: color - A color hex code.
    :param: amt - The amount to lighten up the color; accepts a float with an acceptable range from 0.0 to 1.0.

    :returns: A hex color string of the resulting color.
    """
    return pywal.util.lighten_color(color, amt)


def darker(color, amt):
    """
    Create a darken version of the color.

    :param: color - A color hex code.
    :param: amt - The amount to darken the color; accepts a float with an acceptable range from 0.0 to 1.0.

    :returns: A hex color string of the resulting color.
    """
    return pywal.util.darken_color(color, amt)


def blend_colors(color1, color2):
    """
    Create a new color from adding two colors.

    :param: color1 - A valid hex color string.
    :param: color2 - Another affirmative hexadecimal color code to be combined with the first specified color.

    :returns: A hex color string of the resulting color.
    """
    return pywal.util.blend_color(color1, color2)


def round_to_256_colors(base_color):
    """
    Receives the closest 256 color equivalent of the given color.

    :param: base_color - A Pywal Color instance that'll serve as the base for comparison.

    :returns: A Pywal color instance of the closest 256 color.
    """
    lowest = 3 * (256**2)
    br, bg, bb = [ float(x) for x in base_color.rgb.split(",") ]
    for color in COLORS_256_LIST:
        # The individual RGB of the color.
        r = color["rgb"]["r"]
        g = color["rgb"]["g"]
        b = color["rgb"]["b"]
        distance = (r - br)**2 + (g - bg)**2 + (b - bb)**2
        if distance < lowest:
            lowest = distance
            nearest_color = Color(color["hexString"])
    return nearest_color


def extract_json(json_file_name):
    """
    Simply loads a JSON file as a dictionary.

    :param: json_file_name - The path of the JSON file.

    :returns: The valid JSON file as a dictionary.
    """
    with open(json_file_name) as json_file:
        data = json.load(json_file)
    return data


def wal_to_template(colors):
    """
    Create a new Doom Emacs-compatible color object from a Pywal object.

    :param: colors - The Pywal JSON object.

    :returns: A new Doom Emacs-compatible color object.
    """
    # default colors
    yellow = colors["colors"]["color11"]
    red = colors["colors"]["color1"]
    orange = blend_colors(yellow, red)
    background = colors["special"]["background"]
    foreground = colors["special"]["foreground"]
    if colorsys.rgb_to_hls(*[ float(x) / 255 for x in Color(foreground).rgb.split(",")])[1] > 0.5:
        bases_function = darker
        bg_fg_function = lighter
    else:
        bases_function = lighter
        bg_fg_function = darker
    new_colors = {
        "bg": Color(background),
        "bg-alt": Color(bg_fg_function(background, 0.1)),
        "fg": Color(foreground),
        "fg-alt": Color(bg_fg_function(foreground, 0.2)),
        "base8": Color(bases_function(foreground, 0.1)),
        "base7": Color(bases_function(foreground, 0.2)),
        "base6": Color(bases_function(foreground, 0.3)),
        "base5": Color(bases_function(foreground, 0.4)),
        "base4": Color(bases_function(foreground, 0.5)),
        "base3": Color(bases_function(foreground, 0.6)),
        "base2": Color(bases_function(foreground, 0.7)),
        "base1": Color(bases_function(foreground, 0.8)),
        "base0": Color(bases_function(foreground, 0.9)),
        "red": Color(red),
        "orange": Color(orange),
        "green": Color(colors["colors"]["color2"]),
        "teal": Color(colors["colors"]["color10"]),
        "yellow": Color(yellow),
        "blue": Color(colors["colors"]["color12"]),
        "dark-blue": Color(colors["colors"]["color4"]),
        "magenta": Color(colors["colors"]["color13"]),
        "violet": Color(colors["colors"]["color5"]),
        "cyan": Color(colors["colors"]["color14"]),
        "dark-cyan": Color(colors["colors"]["color6"])
    }
    if new_colors["blue"] == new_colors["dark-blue"]:
        new_colors["dark-blue"] = darker(new_colors["blue"], 0.2)
    if new_colors["cyan"] == new_colors["dark-cyan"]:
        new_colors["dark-cyan"] = darker(new_colors["cyan"], 0.2)
    if new_colors["teal"] == new_colors["green"]:
        new_colors["green"] = darker(new_colors["teal"], 0.2)
    if new_colors["violet"] == new_colors["magenta"]:
        new_colors["magenta"] = darker(new_colors["violet"], 0.2)

    # 256 colors
    new_colors_256 = {}
    for color_name, color in new_colors.items():
        new_colors_256[color_name + "256"] = round_to_256_colors(color)
    new_colors.update(new_colors_256)
    return new_colors


def make_file(output_file, theme, template_path="doom-theme-template.el"):
    """
    Create the Doom Emacs theme file from a template.

    :param: output_file - The file to be written.
    :param: theme - The generated theme object to substitute its value into the template.
    :param: template_path - The template file that serves as the template.

    :returns: Nothing as this is a pure function.
    """
    with open(template_path, "r") as template_file:
        template = template_file.read()
        with open(output_file, "w") as output_file:
            output_file.write(template.format(**theme))


def setup_logging():
    """
    Setup the logger.
    
    :returns: Nothing as the logger is already activated.
    """
    logging.basicConfig(format="[%(levelname)s] %(module)s: %(message)s", level=logging.INFO, stream=sys.stdout)


def setup_args():
    """
    Setup the argument parser.

    :returns: An `argparse.ArgumentParser` object.
    """
    description = "A Doom Emacs theme generator."
    argparser = argparse.ArgumentParser(description=description)

    argparser.add_argument("-i", "--input", metavar = "FILE", help = "The Pywal JSON scheme file (`colors.json`).", type = Path, default = Path(pywal.settings.CACHE_DIR) / "colors.json")
    argparser.add_argument("-o", "--output", metavar = "FILE", help = "The output file of the dynamically generated Doom Emacs theme.")
    argparser.add_argument("-t", "--template", metavar = "FILE", help = "The template of the Doom Emacs theme to be generated.", type = Path, default = Path("./doom-theme-template.el"))
    argparser.add_argument("-n", "--name", help = "The name of the Doom Emacs theme. By default, it is the basename of the 'wallpaper' key from the Pywal JSON object.")

    return argparser


def parse_args(parser, argv):
    """
    Parse the arguments and do the main thing.

    :param: parser - An `argparse.ArgumentParser` instance; usually generated from the `setup_args` function.
    :param: argv - The arguments array.

    :returns: Nada/nothing/zilch.
    """
    global COLORS_256_LIST

    args = parser.parse_args(argv)

    theme = {}

    wal_object = extract_json(args.input)

    if args.name is not None:
        name = args.name
    else:
        name = Path(wal_object["wallpaper"]).stem

    if args.output is not None:
        output_file = args.output
    else:
        output_file = "doom-" + name + "-theme.el"

    COLORS_256_LIST = extract_json("256colors.json")
    colors = wal_to_template(wal_object)

    theme["colors"] = colors
    theme["name"] = name

    make_file(output_file, theme, args.template)


if __name__ == "__main__":
    setup_logging()

    argparser = setup_args()
    parse_args(argparser, sys.argv[1:])
